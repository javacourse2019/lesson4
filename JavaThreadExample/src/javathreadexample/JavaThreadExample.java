
package javathreadexample;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class JavaThreadExample
{
 
    
    public static void main(String[] args)
    {
        // Встроенный в Java сервис выполнения одной нити 
        ExecutorService executor1 = Executors.newSingleThreadExecutor();
        ExecutorService executor2 = Executors.newSingleThreadExecutor();
        
        SendThread jThread1 = new SendThread("Передача данных");
        ReceiveThread jThread2 = new ReceiveThread("Прием данных");
        
        executor1.execute(jThread1);
        executor2.execute(jThread2);


    }
    
}
