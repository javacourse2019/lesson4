
package javathreadexample;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SendThread implements Runnable
{
    
    private String name = "";

    public SendThread(String name)
    {
        this.name= name;
    }

    
    
    @Override
    public void run()
    {
        while (true)  // Начало бесконечного цикла
        {
            LocalDateTime ltd = LocalDateTime.now(); // Получение текущего времени
            try
            {
              
                // Вывод информации о времени и нити на экран
                System.out.println("[" + ltd.toString() +"] "+name+  " " + Thread.currentThread().getId());
                Thread.sleep(3000); // Спать 5 сек 

            } catch (InterruptedException ex)
            {
                Logger.getLogger(SendThread.class.getName()).log(Level.SEVERE, null, ex);
            }

        } // Окончание бесконечного цикла
    }
    
     /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
}
